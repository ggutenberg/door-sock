<?php
/**
 * Sock on the Door
 * Allows one roommate to trigger a Sock on the Door message to be emailed to the other.
 *
 * @todo Native-ify
 * @todo Push Notifications
 */

function get_user() {
    if ( isset( $_COOKIE['sock_user'] ) ) {
		return (array) json_decode( $_COOKIE['sock_user'] );
    }
	return false;
}

function create_cookie( $data ) {
	setcookie( 'sock_user', json_encode( $data ), time() + 60 * 60 * 24 * 365 );
}

$user = false;
if ( ( isset( $_POST['create'] ) || isset( $_POST['send'] ) )
	&& ! empty( $_POST['name'] )
	&& ! empty( $_POST['email'] )
	&& ! empty( $_POST['sender_email'] ) ) {
	$user = $_POST;
	create_cookie( $_POST );
}
elseif ( isset( $_POST['delete'] ) ) {
	setcookie( 'sock_user', '', time() - 3600 );
}
else {
	$user = get_user();
}

//echo '<pre>' . htmlspecialchars( print_r($_POST, true) ) . '</pre>';
//echo '<pre>' . htmlspecialchars( print_r($_COOKIE, true) ) . '</pre>';
?>
<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<title>Sock</title>
		<link rel="shortcut icon" href="favicon.ico" />
		<link rel="apple-touch-icon" href="sock/sizes/sock-144.png" />
		<style>
			body {
				font-size: 20px;
				text-align:center;
			}
			form input, form textarea {
				font-size: 20px;
				margin-bottom: 10px;
				width: 100%;
			}
			textarea {
				height: 4em;
			}
		</style>
		<link rel="stylesheet" href="add2home.css">
		<script type="text/javascript">
			var addToHomeConfig = {
				touchIcon: true
			};
		</script>
		<script type="application/javascript" src="add2home.js"></script>
	</head>
	<body>
		<div id="info">
			<p>Bookmark and load this page to send a sock to your roommate!</p>
		</div>
		<?php
		if ( isset( $_POST['send'] ) && $user ) {
			// Send email, display acknowledgement
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= 'From: ' . 'Door Sock <sysop@dosboy.ca>' . "\r\n";
			$headers .= 'Reply-To: ' . $user['name'] . ' <' . $user['sender_email'] . '>' . "\r\n";
			$headers .= 'Return-Path: ' . $user['name'] . ' <' . $user['sender_email'] . '>' . "\r\n";
			$to = $user['email'];
			$subject = 'Sock on the Door!';
			$message = '
<html>
<head>
	<title>Sock on the Door!</title>
</head>
<body>
	<h1>' . $user['name'] . ' put a sock on the door!</h1>
	<p>' . $user['msg'] . '</p>
	<img src="http://sock.dosboy.ca/sock.gif" />
</body>
</html>
';
			mail( $to, $subject, $message, $headers );
			?>
			<div id="confirmation">
				<p>Sock sent to <?php echo $user['email']; ?>.</p>
				<img src="http://sock.dosboy.ca/sock.gif" />
			</div>
			<?php
		}
		?>
		<form name="sock_form" action="" method="post">
			<input name="name" type="text" placeholder="your name" value="<?php echo $user['name']; ?>" /><br />
			<input name="sender_email" type="email" placeholder="your email" value="<?php echo $user['sender_email']; ?>" /><br />
			<input name="email" type="email" placeholder="roommate's email" value="<?php echo $user['email']; ?>" /><br />
			<textarea name="msg" placeholder="custom message"><?php echo $user['msg']; ?></textarea><br />
			<input name="send" type="submit" value="Hang Sock on Door!" /><br />
			<input name="create" type="submit" value="Provision Sock Recipient" /><br />
			<input name="delete" type="submit" value="Delete Sock Recipient" />
		</form>
	</body>
</html>